package confor.cambiodedivisa;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class FetchTask extends AsyncTask<String, String, Boolean> {
    private URL url;
    private String result;
    private Closure onPreExecuteCallback;
    private Closure onPostExecuteCallback;
    final static int TIMEOUT = 10000; // ms
    final static int SIZE_LIMIT = 1024 * 32; // bytes

    public FetchTask(String inputUrl) throws MalformedURLException {
        this.url = new URL(inputUrl);
    }

    private boolean download() throws IOException {
        HttpURLConnection conn = (HttpURLConnection) this.url.openConnection();
        conn.setConnectTimeout(TIMEOUT);
        conn.connect();

        // debería ser 400 <= CODE < 600
        if (conn.getResponseCode() != 200) {
            Log.e("FetchTask", "non 200 http code: " + conn.getResponseCode());
            return false;
        }

        InputStream stream = conn.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

        StringBuffer buffer = new StringBuffer();
        String line = "";

        while((line = reader.readLine()) != null) {
            buffer.append(line);

            // límite = 16 KiB
            if (buffer.length() >= SIZE_LIMIT) {
                return false;
            }
        }

        conn.disconnect();
        reader.close();
        stream.close();

        this.result = buffer.toString();

        return true;
    }

    public String getResult() {
        return this.result;
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        try {
            Log.i("FetchTask", "downloading " + this.url);
            return this.download();
        } catch (IOException e) {
            Log.e("FetchTask", "Error in FetchTask.doInBackground");
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (this.onPreExecuteCallback != null)
            this.onPreExecuteCallback.exec();
    }

    public void onPreExecute(Closure callback) {
        this.onPreExecuteCallback = callback;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        super.onPostExecute(success);

        if (this.onPostExecuteCallback != null)
            this.onPostExecuteCallback.exec();
    }

    public void onPostExecute(Closure callback) {
        this.onPostExecuteCallback = callback;
    }
}
