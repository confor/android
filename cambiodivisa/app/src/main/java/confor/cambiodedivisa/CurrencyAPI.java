/*
 * para la persona que intente entender este código:
 * es mi peor trabajo hasta la fecha.
 *
 */

package confor.cambiodedivisa;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;

public class CurrencyAPI {
    final private String API_URL = "https://mindicador.cl/api";
    public ArrayList<Currency> currencies; // en pesos chilenos
    private Closure onUpdateCallback;

    private int updated;

    public CurrencyAPI() {
        // valores por defecto (08 de julio, 2019)
        this.currencies = new ArrayList<Currency>();
        this.currencies.add(new Currency("Dólar", "/dolar/", 683.12));
        this.currencies.add(new Currency("Euro", "/euro/", 766.43));
        this.currencies.add(new Currency("UF", "/uf/", 27947.85));
        this.currencies.add(new Currency("UTM", "/utm/", 49033.0));
    }

    public void onUpdate(Closure callback) {
        this.onUpdateCallback = callback;
    }

    private JSONObject parseResponse(String result) {
        if (result == null) {
            Log.e("CurrencyAPI", "no hubo un resultado, o no se que onda");
            return null;
        }

        try {
            JSONObject jsonObj = new JSONObject(result);

            if (jsonObj.has("serie") != true) {
                Log.e("CurrencyAPI", "la api no dio info");
                return null;
            }

            JSONArray info = jsonObj.getJSONArray("serie");

            if (info.length() == 0) {
                Log.e("CurrencyAPI", "no hay informacion para el dia seleccionado");
                return null;
            }

            // `.serie` es un array con info de varios días y horas
            // supongo que sólo importa el último dato del día solicitado
            return info.getJSONObject(0);
        } catch (JSONException e) {
            Log.e("CurrencyAPI", "ta malo el json :c");
            e.printStackTrace();
            return null;
        }
    }

    private void updateCurrency(Currency currency, String date) throws MalformedURLException {
        FetchTask fetch = new FetchTask(API_URL + currency.apiName + date);

        fetch.onPreExecute(() -> {
            Log.i("CurrencyAPI", "actualizando " + currency.name);
        });

        fetch.onPostExecute(() -> {
            JSONObject result = parseResponse(fetch.getResult());
            if (result == null) {
                // ¿deberíamos avisarle sobre el tipo de error al usuario?
                Log.e("CurrencyAPI", "algo falló en lo que dijo la api");
                return;
            }

            try {
                currency.value = result.getDouble("valor");
                Log.i("CurrencyAPI", "el valor de " + currency.name + " para " + date + " es $" + currency.value);
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }

            // contar que ya terminamos de actualizar todas las monedas
            this.updated++;

            if (this.updated == this.currencies.size()) {
                if (this.onUpdateCallback != null)
                    this.onUpdateCallback.exec();
            }
        });

        fetch.execute();
    }

    public void update(String date) throws MalformedURLException {
        // puede ser que alguna de las actualizaciones falle y el
        // codigo quede en una especie de limbo.
        // trabajar con tareas asincronas es super divertido...
        this.updated = 0;

        for (Currency currency : this.currencies) {
            updateCurrency(currency, date);
        }
    }
}
