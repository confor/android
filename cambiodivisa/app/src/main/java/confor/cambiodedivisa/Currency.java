package confor.cambiodedivisa;

public class Currency {
    public String name;
    public String apiName;
    public double value;

    public Currency(String name, String apiName, double value) {
        this.name = name;
        this.apiName = apiName;
        this.value = value;
    }
}
