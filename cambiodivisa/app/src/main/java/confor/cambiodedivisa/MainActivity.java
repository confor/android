package confor.cambiodedivisa;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private String realDate;
    private String wantedDate;
    private CurrencyAPI api;

    private Currency sourceCurrency = new Currency("Peso chileno", "", 1);
    private Currency targetCurrency;

    private EditText value;
    private Spinner currencyOutput;
    private Button submit;
    private Button openCalendar;
    private TextView output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.value = findViewById(R.id.value);
        this.currencyOutput = findViewById(R.id.currency_output);
        this.submit = findViewById(R.id.submit);
        this.openCalendar = findViewById(R.id.open_calendar);
        this.output = findViewById(R.id.output);

        this.realDate = "08-07-2019";
        this.wantedDate = "09-07-2019";
        this.api = new CurrencyAPI();

        listeners();
        updateCurrenciesUI();

        try {
            // esto corre CADA VEZ que la interfaz se crea.
            // android crea toda la interfaz de nuevo cuando rotamos la pantalla,
            // por lo que se actualizan las monedas al rotar la pantalla.
            this.updateCurrencies();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    private void listeners() {
        this.submit.setOnClickListener(this::onSubmit);

        this.openCalendar.setOnClickListener((View view) -> {
            // fuente: https://stackoverflow.com/a/31279458/4301778
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.datepicker);

            CalendarView calendarView = dialog.findViewById(R.id.calendar);
            calendarView.setMaxDate(Calendar.getInstance().getTimeInMillis());

            Button accept = dialog.findViewById(R.id.accept);
            accept.setOnClickListener((View v) -> {
                try {
                    this.updateCurrencies();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            });

            Button cancel = dialog.findViewById(R.id.cancel);
            cancel.setOnClickListener((View v) -> {
                dialog.dismiss();
            });

            // poner la fecha de ahora por defecto
            calendarView.setDate(Calendar.getInstance().getTimeInMillis() - 1);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            wantedDate = formatter.format(Calendar.getInstance().getTime());

            calendarView.setOnDateChangeListener((CalendarView v, int year, int month, int day) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

                if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
                    showToast("No hay información financiera en fines de semana");
                    accept.setEnabled(false);
                } else {
                    accept.setEnabled(true);
                }

                // cambiar a la fecha seleccionada
                wantedDate = String.format("%02d-%02d-%04d", day, month + 1, year);
            });


            dialog.show();
        });

        this.currencyOutput.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setTargetCurrency(api.currencies.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
    }

    private void updateCurrencies() throws MalformedURLException {
        if (this.realDate.equals(this.wantedDate)) {
            showToast("Monedas ya actualizadas (" + this.wantedDate + ")");
            return;
        }

        showToast("Actualizando monedas (" + this.wantedDate + ")...");

        api.onUpdate(() -> {
            showToast("¡Monedas actualizadas!");
            updateCurrenciesUI();

            this.realDate = this.wantedDate;
        });

        api.update(this.wantedDate);
    }

    public void onSubmit(View view) {
        String text = this.value.getText().toString();

        if (text.equals("")) {
            showToast("Escribe un valor!");
            return;
        }

        if (this.sourceCurrency == null) {
            showToast("Selecciona una moneda de origen");
            return;
        }

        if (this.targetCurrency == null) {
            showToast("Selecciona una moneda de destino");
            return;
        }

        try {
            Long.parseLong(text, 10);
        } catch (NumberFormatException e) {
            showToast("La cantidad ingresada es inválida");
            return;
        }

        double source = Long.parseLong(text, 10) * this.sourceCurrency.value;
        double target = this.targetCurrency.value;

        double calc = source / target;
        this.output.setText("$" + String.format("%.0f", source) + " a " + this.targetCurrency.name + " = " + String.format("%.2f", calc) + "\n(datos de " + realDate + ")");
    }

    private void updateCurrenciesUI() {
        ArrayList<String> keys = new ArrayList<String>();

        for (Currency currency : this.api.currencies) {
            keys.add(currency.name);
        }

        ArrayAdapter<String> targetAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, keys);
        targetAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.currencyOutput.setAdapter(targetAdapter);
    }

    public Currency getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(Currency targetCurrency) {
        this.targetCurrency = targetCurrency;
    }
}
